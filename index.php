<!-- 
URL de la doc de base de Google Map API JS
https://developers.google.com/maps/documentation/javascript/
-->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Démo Google Map</title>
	<!-- chargement api google map JS, avec clé d'api. Pour obtenir une clé d'api il faut avoir un compte Google développeur, créer un projet, une clé, et activer pour cette clé les api correspondantes (en l'occurrence Google Map Javascript API) -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=TaCléIci"></script>
	<script type="text/javascript" src="infobubble.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
	<script type="text/javascript">

		// On récupère les points sous la forme d'un objet json
		var donnees = <?php include("points.php"); ?>;

		
		$(document).ready(function(){
			// log pour voir les données qu'on récupère du JSON
			console.log(donnees);
			// exemples de récupération de propriétés du premier objet
			console.log(donnees[0].nom);
			console.log(donnees[0].lat);
			console.log(donnees[0].lng);


			// Fonction d'initialisation de la map
			// Instanciation d'un nouvel objet de la classe Map, placé dans l'élément portant l'id #map dans notre page.
	        /*
	        Documentation de la classe Map :
	        https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=fr

			sur cette page de doc on va trouver les différentes propriétés et méthodes de la classe, servant à la paramétrer mais aussi à éxécuter des actions en réponse à des événements.

	        */


	        // On initialise la map
	        var map = "";

			function initMap() {
		        map = new google.maps.Map(document.getElementById('map'), {
		        	// Paramètres généraux de la map
					center: {lat: 45.7465684, lng: 4.83842089999996},
					zoom: 11,
					clickableIcons: false
		        });
	      	}

			initMap();

			

			// Maintenant que la map est créée, on va boucler sur notre tableau JSON pour en extraire tous les objets, et surtout leurs coordonnées GPS.

			// on instancie un tableau markers pour être utilisé dans la boucle ci-dessous
			var markers = [];

			// Définition des pictos marqueurs
			var picto = 'picto.png';

			// On utilise la boucle for in de JS, comparable à la boucle foreach en PHP
			for(index in donnees) {

				var point = donnees[index];

				// On va récupérer le positionnement GPS de chaque objet et en créer un objet de la classe LatLng

				// Pour chaque élément trouvé, on va créer un marqueur qu'on va stocker dans un tableau Markers créé plus tôt.

				var myLatLng = new google.maps.LatLng(point.lat, point.lng);

				markers[index] = new google.maps.Marker({
				    position: myLatLng,
				    title: point.nom,
				    icon: picto
			  	});

			  	// on place le marqueur sur la carte
			  	markers[index].setMap(map);

			  	// on crée le contenu de la fenêtre d'infos
			  	markers[index].contenu = "<h1>"+point.nom+"</h1><p>"+point.adresse+"<br />"+point.codePostal+" "+point.ville+"<br />"+point.tel+"<br /></p>";

			  	
			  	// on écoute l'événement click sur le marker afin d'ouvrir l'infowindow correspondante
				google.maps.event.addListener(markers[index], 'click', function () {
					if (!infos.isOpen()) {
						infos.setContent(this.contenu);
			            infos.open(map, this);
			            console.log(infos);
			            console.log(infos.bubble_);
			            $(infos.bubble_).addClass("maClasse");
			            $(infos.contentContainer_).addClass("container");
			        }
			  		
			  	});

			  	// On va créer la fenêtre d'infos
			  	/*var infos = new google.maps.InfoWindow({
				    content: "A charger..."
			  	});*/

				var infos = new InfoBubble({
					map: map,
					backgroundClassName: "bulle",
					maxWidth: 400,
					minWidth: 400,
					shadowStyle: 0
				});


			}


		});


	</script>
	<style type="text/css">
	.maClasse {
		font-size: 36px;
		background-color: #e8e8e8;
		opacity: 0.9;
		width: 380px;
		height: 300px;

	}
	.container {
		background-color: #ff0000;
		height: 300px;
		min-height: 300px;
	}
	.bulle {
		width: 380px;
		height: 300px;
		display: block;
		position: absolute;
	}
	</style>
</head>
<body>
	<h1>Démo Google Maps</h1>
	<div id="map" style="width: 100%; height: 400px;">
	</div>
	<p>
	Pour enrichir cet exemple :<br />
	<ul>
		<li><a href="https://github.com/googlemaps/">Dépôt Github Google Map Api</a>
			<ul>
				<li>
					<a href="https://github.com/googlemaps/android-maps-utils">Bibliothèque complète</a>
				</li>
				<li>
					<a href="https://github.com/googlemaps/js-rich-marker">Rich Marker : ajouter des infos dans le marker</a>
				</li>
				<li>
					<a href="https://github.com/googlemaps/js-info-bubble">Info Bubble : le marker devient un élément DOM classique donc facilement stylisable via CSS</a>
				</li>
				<li>
					<a href="https://github.com/googlemaps/js-marker-clusterer">Clusterisation</a>
				</li>
			</ul>
		</li>

	</ul>

	</p>
</body>
</html>