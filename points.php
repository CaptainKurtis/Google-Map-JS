<?php
$points = array(
	array (
		"nom" => "Un restaurant",
		"lat" => "45.7465684",
		"lng" => "4.83842089999996",
		"adresse" => "39 Avenue Berthelot",
		"codePostal" => "69007",
		"ville" => "Lyon",
		"tel" => "01 02 03 04 05",
		"type" => "principal"
	),
	array (
		"nom" => "Clément",
		"lat" => "45.7161555",
		"lng" => "4.805408199999988",
		"adresse" => "31 rue de la Commune de Paris",
		"codePostal" => "69600",
		"ville" => "Oullins",
		"tel" => "01 02 03 04 05",
		"type" => "point"
	),
	array (
		"nom" => "Les Pixels Associés",
		"lat" => "45.9074581",
		"lng" => "5.193310500000052",
		"adresse" => "15 allée du chateau",
		"codePostal" => "01800",
		"ville" => "Meximieux",
		"tel" => "01 02 03 04 05",
		"type" => "point"
	)
);

echo json_encode($points);


?>
